// FB initiation function
window.fbAsyncInit = () => {
  FB.init({
    appId      : '1854891768155727',
    cookie     : true,
    xfbml      : true,
    version    : 'v2.11'
  });

  FB.getLoginStatus(function(response) {
    if (response.status === 'connected') {

      // the user is logged in and has authenticated your
      // app, and response.authResponse supplies
      // the user's ID, a valid access token, a signed
      // request, and the time the access token
      // and signed request each expire
      console.log(response)
      var uid = response.authResponse.userID;
      var accessToken = response.authResponse.accessToken;
      render(true);
    } else if (response.status === 'not_authorized') {
      // the user is logged in to Facebook,
      // but has not authenticated your app
      console.log(response)
      render(false );
    } else {
      // the user isn't logged in to Facebook.
      console.log(response)
      render(false);
    }
   });

  // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
  // dan jalankanlah fungsi render di bawah, dengan parameter true jika
  // status login terkoneksi (connected)

  // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
  // otomatis akan ditampilkan view sudah login

};

// Call init facebook. default dari facebook
(function(d, s, id){

   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement(s); js.id = id;
   js.src = "https://connect.facebook.net/en_US/sdk.js";
   fjs.parentNode.insertBefore(js, fjs);
 }(document, 'script', 'facebook-jssdk'));


const facebookLogin = () => {
    FB.login(function(response){
      console.log(response);
      render(true);
  }, {scope:'public_profile,user_posts,publish_actions,user_about_me,email'})

  // TODO: Implement Method Ini
  // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
  // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
  // pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.
};

// Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
// merender atau membuat tampilan html untuk yang sudah login atau belum
// Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
// Class-Class Bootstrap atau CSS yang anda implementasi sendiri
const render = (loginFlag) => {

  if (loginFlag) {


    // Jika yang akan dirender adalah tampilan sudah login

    // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
    // yang menerima object user sebagai parameter.
    // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
    //console.log(response);
    //console.log(id)
    getUserData(user => {
      // Render tampilan profil, form input post, tombol post status, dan tombol logout
      //console.log(user);
      $('#lab8').html(
        '<div class="profile">' +
            '<div class="img-container">'+
              '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
              '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
              '<h1 class="name">' + user.name + '</h1>' +
              '</div>'+
              '<div class="dataPost">'+
              '<div class="data" id="left">' +
            '<h3 id="about">' + 'About me: ' + user.about + '</h2>' +
            '<h3 id="email">' + 'Email: '+user.email + '</h3>' +
            '<h3 id="gender">' + 'Gender: '+user.gender + '</h3>' +
          '</div>' +'<div class="post" id="right">' +
        '<textarea id="postInput"class="post" placeholder="Ketik Status Anda" ></textarea> ' +
        '<button class="postStatus" onclick="postStatus()">Post ke Facebook</button>' +
        '<button class="logout" onclick="facebookLogout()">Logout</button>'+
        '</div>'+
        '<div class="clear"></div>'+
        '</div>'+
        '</div>'

      );
      $('#lab8').append(
          '<h1 id = "feedHeader"> Your Feed </h1>'
      );

      // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
      // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
      // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
      // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
      getUserFeed(feed => {
        feed.data.map(value => {
          // Render feed, kustomisasi sesuai kebutuhan.
          if (value.message && value.story) {
            $('#lab8').append(
              '<div class="feed data-aos="fade-down"">' +
                '<h5>' + value.message + '</h5>' +
                '<h6>' + value.story + '</h6>' +
              '</div>'
            );
          } else if (value.message) {
            $('#lab8').append(
              '<div class="feed" data-aos="fade-down">' +
                '<h5>' + value.message + '</h5>' +
              '</div>'
            );
          } else if (value.story) {
            $('#lab8').append(
              '<div class="feed" data-aos="fade-down">' +
                '<h5>' + value.story + '</h5>' +
              '</div>'
            );
          }
        });
      });
    });
  } else {
    // Tampilan ketika belum login
    $('#lab8').html('<button class="login" onclick="facebookLogin()">Login ke Facebook</button>');

  }
};




const facebookLogout = () => {
  // TODO: Implement Method Ini
  // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
  // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.

  FB.getLoginStatus(function(response) {
   if (response.status === 'connected') {
       alertify.confirm("Loggin out?", function (e) {
         if (e) {
             FB.logout();render(false);
             alertify.success("Logged out")
         } else {
             alertify.error("Logout canceled");
             // user clicked "cancel"
         }
     });

   }

   //$('#lab8').remove();
   //location.reload();
});

};

// TODO: Lengkapi Method Ini
// Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
// lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di
// method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan
// meneruskan response yang didapat ke fungsi callback tersebut
// Apakah yang dimaksud dengan fungsi callback?
const getUserData = (fun) => {

  FB.api('/me',{fields: 'name,email, picture, cover, gender, about'}, 'GET', function (response){
      console.log(response);
      picture = response.picture.data.url;
      name = response.name;
      email = response.email;
      console.log(email);
    fun(response);
  });

};

const getUserFeed = (fun) => {
    /* make the API call */
    FB.api(
        "/me/feed",
        function (response) {
          if (response && !response.error) {
            /* handle the result */
            console.log(response);
            fun(response);
          }
        }
    );
  // TODO: Implement Method Ini
  // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
  // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
  // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
  // tersebut
};

const postFeed = (pesan) => {
  // Todo: Implement method ini,
  // Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
  // Melalui API Facebook dengan message yang diterima dari parameter.
  console.log("ini" + pesan);
  var message = pesan;
  FB.api('/me/feed', 'POST', {message:message});
  //window.location.reload();
  alertify.confirm("Upload this post?", function (e) {
    if (e) {
        window.location.reload();
        alertify.success("Post success");
    } else {
        alertify.error("Post fail");
        // user clicked "cancel"
    }
});

};

const postStatus = () => {
  const message = $('#postInput').val();
  console.log(message);
  postFeed(message);
};
